CXX=g++
CXXFLAGS=-std=c++17 -O2
CPPFLAGS=
LDFLAGS=

.PHONY: clean

main: main.o
	$(CXX) -o $@ $^ $(LDFLAGS)

main.o: main.cpp
	$(CXX) $(CXXFLAGS) -o $@ -c $< $(CPPFLAGS)

clean:
	rm -f main *.o
